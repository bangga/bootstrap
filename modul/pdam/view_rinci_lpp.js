dataProc	= {};
targetUrl   = 'http://10.8.0.13/api/pdam/get_lpp.php';
jQuery.post(targetUrl, {}, function(data){
	jQuery.each(data.data,function(i,value){
		dataProc[value.rek_nomor]	= {proses: "rinci_" + dataTemp.applKode, kubikasi: value.rek_stanpakai, biayaAir: value.rek_uangair, biayaBeban: value.rek_beban, denda: value.rek_denda};
		inHTML	= '<tr onclick="nonghol(' + value.rek_nomor + ')" style="cursor: pointer">' +
						'<td align="center">' + value.pel_no + '</td>' +
						'<td>' + value.pel_nama + '</td>' +
						'<td align="center">' + value.dkd_kd + '</td>' +
						'<td align="right">' + value.rek_bln + '</td>' +
						'<td align="center">' + value.rek_thn + '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'de'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_denda, {format:'#,###', locale:'de'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.byr_total, {format:'#,###', locale:'de'}) + '</td>' +
						'<td>' + value.kar_nama + '</td>' +
						'<td>' + value.byr_upd_sts + '</td>' +
					'</tr>';
		jQuery('#tabel-lpp tbody').append(inHTML);
	});

	jQuery(document).ready(function(){
		jQuery('#tabel-lpp').dataTable({
			stateSave: true
		});
	});

}, "json");

function nonghol(param){
	jQuery('#myModal').modal('toggle');
	dataTemp.procKode	= param;
	dataFeed			= dataTemp.proses[param];
	localStorage.setItem("tirtasakti", JSON.stringify(dataTemp));
	jQuery('input[name=rek_pakai]').attr('placeholder', jQuery.formatNumber(dataFeed.kubikasi, {format:'#,###', locale:'de'}) + ' M3');
	jQuery('input[name=rek_uangair]').attr('placeholder', jQuery.formatNumber(dataFeed.biayaAir, {format:'#,###', locale:'de'}));
	jQuery('input[name=rek_beban]').attr('placeholder', jQuery.formatNumber(dataFeed.biayaBeban, {format:'#,###', locale:'de'}));
	jQuery('input[name=rek_denda]').attr('placeholder', jQuery.formatNumber(dataFeed.denda, {format:'#,###', locale:'de'}));
}

// pengaturan halaman sunting
jQuery('#button-sunting').click(function(){
	jQuery('input').attr('disabled', false);
	jQuery('select').attr('disabled', false);
	jQuery('.sunting').removeClass('hidden');
	jQuery('.rincian').addClass('hidden');
});

jQuery('#button-batal').click(function(){
	jQuery('input').attr('disabled', true);
	jQuery('select').attr('disabled', true);
	jQuery('.sunting').addClass('hidden');
	jQuery('.rincian').removeClass('hidden');
});

// storing kontrol proses
jQuery.extend(dataTemp, {proses: dataProc});
localStorage.setItem("tirtasakti", JSON.stringify(dataTemp));
console.log(dataTemp);