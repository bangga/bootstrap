jQuery(function() {
	var data	= {data : [{"periode":"2013-10","drd":"1234851520","saldo":"306396980","tunggakan":"1419679615"},{"periode":"2013-11","drd":"1176146800","saldo":"321060860","tunggakan":"1492376135"},{"periode":"2013-12","drd":"1465010920","saldo":"469563280","tunggakan":"1542794475"},{"periode":"2014-01","drd":"1422004240","saldo":"323526560","tunggakan":"1611706955"},{"periode":"2014-02","drd":"1512280960","saldo":"314957880","tunggakan":"1773524240"},{"periode":"2014-03","drd":"1498563780","saldo":"292486620","tunggakan":"1785442775"},{"periode":"2014-04","drd":"1421780360","saldo":"280469600","tunggakan":"1745168325"},{"periode":"2014-05","drd":"1563596040","saldo":"303623600","tunggakan":"1723793545"},{"periode":"2014-06","drd":"1566728540","saldo":"291797220","tunggakan":"1713886845"},{"periode":"2014-07","drd":"1508187060","saldo":"333094040","tunggakan":"1811812930"},{"periode":"2014-08","drd":"1523068980","saldo":"252486660","tunggakan":"1818368210"},{"periode":"2014-09","drd":"1834244280","saldo":"326945580","tunggakan":"1827803265"}]};

	// var targetUrl = "http://10.8.0.13/api/pdam/get_rekap_akhir_bulan.php";
	// jQuery.post(targetUrl, {}, function(data){
		Morris.Area({
			element: 'morris-area-chart',
			data: data.data,
			xkey: 'periode',
			ykeys: ['drd', 'saldo', 'tunggakan'],
			labels: ['DRD', 'Saldo', 'Tunggakan'],
			pointSize: 2,
			hideHover: 'auto',
			resize: true
		});
	// }, 'json');



    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Download Sales",
            value: 12
        }, {
            label: "In-Store Sales",
            value: 30
        }, {
            label: "Mail-Order Sales",
            value: 20
        }],
        resize: true
    });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: '2006',
            a: 100,
            b: 90
        }, {
            y: '2007',
            a: 75,
            b: 65
        }, {
            y: '2008',
            a: 50,
            b: 40
        }, {
            y: '2009',
            a: 75,
            b: 65
        }, {
            y: '2010',
            a: 50,
            b: 40
        }, {
            y: '2011',
            a: 75,
            b: 65
        }, {
            y: '2012',
            a: 100,
            b: 90
        }],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true
    });

});
