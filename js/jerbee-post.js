function getToken(){
    var randId = new Date();
    return randId.getTime();
}

function getParams(opt){
    var stringData	= '{';
    var	kunci		= false;
    var nilai		= false;
    var targetUrl   = false;
    var targetId	= false;
    var errorId		= false;
    var	params		= document.getElementsByClassName(opt);
    for(i=0;i<params.length;i++){
        kunci = params[i].name;
        nilai = params[i].value;
        if(kunci=='targetId'){
            targetId = nilai;
        }
        if(kunci=='targetUrl'){
            targetUrl = nilai;
        }
        stringData = stringData+'\"'+kunci+'\"'+':\"'+nilai+'\"';
        if((i+1)<params.length){
            stringData = stringData+',';
        }
    }
    if(i>0 && targetId==false){
        targetId 	= getToken();
        stringData 	= stringData+',"targetId\":"'+targetId+'\"}';
    }
    else{
        stringData 	= stringData+'}';
    }
    return new Array(targetUrl,targetId,stringData);
}

function login(param){
    var params		= getParams(param);
    var targetUrl   = params[0];
    var targetId    = params[1];
    var data        = JSON.parse(params[2]);
    jQuery.post(targetUrl, data, function(data) {
        localStorage.setItem("loginToken", data.loginToken);
        localStorage.setItem("loginUser", data.loginUser);
        window.location.assign("index.html");
    }, "json");
}

