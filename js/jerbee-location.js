if (navigator.geolocation) {
	navigator.geolocation.watchPosition(function(posisi){
		var targetUrl   = "/api/mmr_update_location.php";
		var loginUser   = "000000";
		var data	    = {kar_id:loginUser, latkor:posisi.coords.latitude, lonkor:posisi.coords.longitude, akurasi:posisi.coords.accuracy, altitud:Number(posisi.coords.altitude), alt_akurasi:Number(posisi.coords.altitudeAccuracy), arah:Number(posisi.coords.heading), kecepatan:Number(posisi.coords.speed), akses_tgl:posisi.timestamp};
		// latkor,lonkor,akurasi,altitud,alt_akurasi,arah,kecepatan,akses_tgl
		// coords.latitude	        The latitude as a decimal number
		// coords.longitude	        The longitude as a decimal number
		// coords.accuracy	        The accuracy of position
		// coords.altitude	        The altitude in meters above the mean sea level
		// coords.altitudeAccuracy	The altitude accuracy of position
		// coords.heading	        The heading as degrees clockwise from North
		// coords.speed	        The speed in meters per second
		// timestamp
		jQuery.post(targetUrl, data, function(data) {
			if(data.errno==1){
				alert(data.pesan);
			}
		}, "json");
	});
}
else {
    alert("Geolocation is not supported by this browser");
}
