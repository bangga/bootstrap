jQuery.ajaxSetup ({
	xhrFields: {
	  withCredentials: false
	},
    cache: false
});

function toggleMenu(){
	var tampil = jQuery('.sidebar').is(':visible');
	jQuery('.sidebar').fadeToggle('slow',function(){
		if(tampil==true){
			jQuery('#page-wrapper').css('margin','0px');
		}
	});
	if(tampil==false){
		jQuery('#page-wrapper').css('margin','0 0 0 250px');
	}
}

// load menu utama
function loadMenu(param){
	dataFeed			= dataTemp[param];

	// set menu aktif
	targetUrl			= dataFeed.targetUrl + 'index.html';
	dataTemp.applKode	= param;
	if(typeof dataTemp.procKode == 'string'){
		delete dataTemp.procKode;
	}
	if(typeof dataTemp.proses == 'object'){
		delete dataTemp.proses;
	}
	localStorage.setItem('bootstrap', JSON.stringify(dataTemp));
	jQuery('#page-content').load(targetUrl);
}

// load proses in-menu
function loadFile(param){
	dataTemp	= JSON.parse(localStorage.bootstrap);
	dataFeed	= {proses: dataTemp.proses[param].proses};
	targetUrl	= dataTemp[dataTemp.applKode].targetUrl + 'index.html';;

	// set proses aktif
	dataTemp.procKode	= param;
	localStorage.setItem('bootstrap', JSON.stringify(dataTemp));

	jQuery('.block').load(targetUrl);
}

var kunci		= "";
var inHTML		= "";
var dataTemp	= {};
var targetUrl	= "./data/menu.txt";

// jQuery.post(targetUrl, {}, function(data){
jQuery.get(targetUrl, function(data){
	if(data.errno==2){
		document.location.href = './login.html';
	}
	else{
		// Nav first level
		jQuery.each(data, function(key, value){
			kunci = key;
			jQuery.each(value, function(key, value){
				inHTML	= '';
				if(kunci=='000000'){
					if(value.appl_kode.substr(4,2)=='00'){
						inHTML = '<li><a><i class="' + value.appl_icon + '"></i> ' + value.appl_name + '<span class="fa arrow"></span></a><ul class="' + value.appl_kode + ' nav nav-second-level"></ul></li>';
					}
					else{
						dataTemp[value.appl_kode]	= {targetUrl: value.appl_file, applKode: value.appl_kode, applName: value.appl_name, applDesc: value.appl_deskripsi};
						inHTML = '<li><a onclick="loadMenu(\'' + value.appl_kode + '\')"><i class="' + value.appl_icon + '"></i> ' + value.appl_name + '</a></li>';
					}
					jQuery('.' + kunci).append(inHTML);
					delete data[kunci];
				}
			});
		});

		// Nav second level
		jQuery.each(data, function(key, value){
			kunci = key;
			jQuery.each(value, function(key, value){
				inHTML	= '';
				if(value.appl_kode.substr(4,2)=='00'){
					inHTML	= '<li><a>' + value.appl_name + ' <span class="fa arrow"></span></a>' +
								'<ul class="' + value.appl_kode + ' nav nav-third-level"></ul>' +
							'</li>';
				}
				else{
					dataTemp[value.appl_kode]	= {targetUrl: value.appl_file, applKode: value.appl_kode, applName: value.appl_name, applDesc: value.appl_deskripsi};
					inHTML = '<li><a onclick="loadMenu(\'' + value.appl_kode + '\')"><i class="' + value.appl_icon + '"></i> ' + value.appl_name + '</a></li>';
				}
				jQuery('.' + kunci).append(inHTML);
				delete data[kunci];
			});
		});
	}
	localStorage.setItem('bootstrap', JSON.stringify(dataTemp));
	jQuery.getScript('js/sb-admin-2.js');
}, "json");
